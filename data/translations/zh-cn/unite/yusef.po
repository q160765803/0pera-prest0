# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, Kenneth Maage <kennethm@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-08-31 11:13-02:00\n"
"PO-Revision-Date: 2009-10-27 02:57+0800\n"
"Last-Translator: Wood <zhanwu.opera@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"

#. A link to preview an application as a visitor.
#: TO BE ADDED
msgid "Preview as visitor"
msgstr "以访客身份预览"

#. A link to stop previewing an application as a visitor and
#. go back to 'owner mode'.
#: TO BE ADDED
msgid "Exit visitor preview"
msgstr "退出预览"

#. A link to go to the owner's Home service and view his applications.
#: TO BE ADDED
msgid "My applications"
msgstr "我的应用程序"

#. A link to go to view the owner's friends' applications.
#: TO BE ADDED
msgid "My friends' applications"
msgstr "我的朋友的应用程序"

#. A link for visitors to go to the owner's Home service and view his applications.
#: TO BE ADDED
msgid "{username}'s applications"
msgstr "{username}的应用程序"

#. A link for visitors to go to view the owner's friends' applications.
#: TO BE ADDED
msgid "{username}'s friends' applications"
msgstr "{username}的朋友的应用程序"

#. A link for the owner to edit his message to his visitors.
#: TO BE ADDED
msgid "Edit this text"
msgstr "编辑此处文本"

#. A heading for the Sharing section in an application.
#: TO BE ADDED
msgid "Sharing"
msgstr "共享"

#. A sentence that suggests to a user to share via E-mail/Facebook/Twitter/Delicious.
#. Eg. Share via: [icon for e-mail] [icon for facebook] [icon for twitter] [icon for delicious]
#: TO BE ADDED
msgid "Share via:"
msgstr "共享方式："

#. An alternative text for a button that shares the application link via e-mail.
#: TO BE ADDED
msgid "Send via e-mail"
msgstr "使用电邮发送"

#. An alternative text for buttons (alt='') that shares the application link on
#. Facebook/Twitter. Examples: 'Share on Facebook', 'Share on Twitter'.
#: TO BE ADDED
msgid "Share on "
msgstr "共享于"

#. An alternative text for a button (alt='') that saves the application link on
#. Delicious. Example: 'Save to Delicious'.
#: TO BE ADDED
msgid "Save to Delicious"
msgstr "保存到 Delicious"

#. A heading for a section that describes what Opera Unite is.
#: TO BE ADDED
msgid "What is Opera Unite?"
msgstr "什么是 Opera Unite？"

#. A paragraph of a text that describes what Opera Unite is.
#: TO BE ADDED
msgid "Opera Unite is a new way to share, connect and collaborate directly between computers across the Web."
msgstr "Opera Unite 是一种全新的网际共享、连接和协作方式。"

#. A paragraph of a text that describes what Opera Unite is.
#: TO BE ADDED
msgid "This Web page comes directly from {username}'s personal computer."
msgstr "这个网页直接来自于{username}的个人电脑。"

#. A link for users that want to learn more about Opera Unite. It links to unite.opera.com.
#: TO BE ADDED
msgid "Learn more"
msgstr "了解更多"

#. A heading for the privacy settings for the application.
#: libraries/yusef/plugins/acl/acl.js
msgid "Privacy options for {serviceName}"
msgstr "关于{serviceName}的隐私选择"

#. Label on the radio button controlling the privacy settings for an application, when a password is required
#: libraries/yusef/plugins/acl/acl.js
msgid "Password protected"
msgstr "私人"

#. Description of the privacy settings for an application, when a password is required (Password protected)
#: libraries/yusef/plugins/acl/acl.js
msgid "Visitors must know the password"
msgstr "访客必须知道密码才能访问"

#. Label on the radio button controlling the privacy settings for an application, when no password is required
#: libraries/yusef/plugins/acl/acl.js
msgid "Public"
msgstr "公共"

#. Description of the privacy settings for an application, when no password is required (Public)
#: libraries/yusef/plugins/acl/acl.js
msgid "Anyone may visit your {serviceName}"
msgstr "所有人都可以访问您的{serviceName}"

#. A button that saves the values of the form for privacy settings.
#: TO BE ADDED
msgid "Save changes"
msgstr "保存"

#. A feedback message that notifies the user to have a longer password.
#: TO BE ADDED
msgid "For your own security, please enter at least 4 letters or numbers."
msgstr "为了您的安全，请输入至少4位数字。"

#. The footer text of every page in an Opera Unite application.
#. The complete text is 'Powered by Opera Unite', but Opera Unite
#. is not to be translated.
#: TO BE ADDED
msgid "Powered by"
msgstr "Powered by"

#. Label for the access control setting where a password is required
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Password protected"
msgstr "私人"

#. Label text for the password associated with the limited access setting
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Password:"
msgstr "密码："

#. Button text to confirm the password entered
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Log in"
msgstr "登入"

#. Message to a visitor who does not enter the correct password
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "The password was incorrect. Try again, or contact <A href=\"http://my.opera.com/{username}/\"><EM>{username}</EM> for the password.</A>."
msgstr "密码不正确。请重试，或者联系<A href=\"http://my.opera.com/{username}/\"><EM>{username}</EM>索取密码。</A>."

#. Error message about cookies
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Cookies are required for authentication. If you have enabled them, <A href=\"http://{hostName}{servicePath}/{requestSection}/{requestPath}\">please click here to reload this page and try again.</A>"
msgstr "身份验证需要 Cookie。如果您已经启用了 Cookie，<A href=\"http://{hostName}{servicePath}/{requestSection}/{requestPath}\">请单击这里以重载此页面。</A>"

#. Greeting portion of the message to visitors when access is set to Limited
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Dear visitor,"
msgstr "其爱的访客，"

#. Message to visitors when access is set to Limited
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "You can only visit this application if {username} has given you the password."
msgstr "只有{username}给您密码的时候i您才能访问。"

#. Wrapup portion of the message to visitors when access is set to Limited.
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "You can contact <A href=\"http://my.opera.com/{username}/\"><EM>{username}</EM></A> to ask for the password."
msgstr "您可以联系<A href=\"http://my.opera.com/{username}/\"><EM>{username}</EM></A>索取密码。"

#. Warning header when a password is short
#: libraries/yusef/plugins/acl/acl.js
msgid "Short password"
msgstr "短密码"

#. Warning message when a password is short
#: libraries/yusef/plugins/acl/acl.js
msgid "For your own security, please enter at least 4 letters or numbers."
msgstr "为了您的安全，请输入至少4位数字。"

#. Warning header when the password is included in the link
#: libraries/yusef/plugins/acl/acl.js
msgid "Password attached to link"
msgstr "链接包含密码"

#. Warning message when the password is included in the link
#: libraries/yusef/plugins/acl/acl.js
msgid "If a visitor forwards the link to a stranger, the stranger will also have access."
msgstr "如果访客将链接转发给陌生人，陌生人也将拥有访问权限。"

#. Label text for option to attach the password to the sharing link
#: TO BE ADDED
msgid "Attach password to the Sharing link"
msgstr "将密码包含到共享链接"

#. Description text for option to attach the password to the sharing link
#: TO BE ADDED
msgid "Lets friends have access by clicking on a single link. Use with caution."
msgstr "朋友们只需一点就可以轻松访问。慎用。"

#. Description text for option to attach the password to the sharing link
#: TO BE ADDED
msgid "Invalid password. Allowed characters are a-z, A-Z and 0-9."
msgstr "无效密码。允许字符 a-z, A-Z 和 0-9。"

#. A heading to a template error page that shows when a user is trying to go to an incorrect URL.
#: TO BE ADDED
msgid "Page not found"
msgstr "页面不存在"

#. A sentence that comes before suggestions of what a user can do to correct the error.
#. Eg. 'Try any of the following:
#. Option 1
#. Option 2
#: TO BE ADDED
msgid "Try any of the following:"
msgstr "尝试下面的一种办法："

#. A suggestion of what a user can do to correct an error.
#: TO BE ADDED
msgid "make sure the Web address is spelled correctly"
msgstr "确认网址拼写正确"

#. A suggestion of what a user can do to correct an error.
#: TO BE ADDED
msgid "return to <A href=\"{servicePath}/\">the start page of {username}'s {serviceName}</A>"
msgstr "返回<A href=\"{servicePath}/\">{username}的{serviceName}的开始页面</A>"

#. Text shown to users who have either turned off Javascript, or are using a browser that does not support it. The application will work without javascript, though it will lose some of its advanced features.
#: libraries/yusef/plugins/ui/skins/default/main.html
msgid "Please enable JavaScript to use all features of this application."
msgstr "要使用此应用程序的全部特性，请启用 JavaScript。"

