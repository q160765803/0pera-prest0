# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, Kenneth Maage <kennethm@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-08-31 11:13-02:00\n"
"PO-Revision-Date: 2009-10-27 23:35+0200\n"
"Last-Translator: Aliaksei <flybelarus@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Notification message shown on the computer running the PhotoSharing when new thumbnails and preview images are generated, which might be intensive.
#: photosharing.js
msgid "Generating thumbnails. Your computer may slow down."
msgstr ""

#. Error heading when a link is misspelled or the photo or album doesn't exist.
#: templates/resourceNotFound.html
msgid "Folder or photo not found."
msgstr "Тэчка або здымак ня знойдзеныя."

#. Link that navigates to the picture before this one.
#: templates/index.html
msgid "Previous"
msgstr "Папярэдні"

#. Gray text shown in place of "Previous" link, when the picture is the first one in an album
#: templates/index.html
msgid "First"
msgstr "Першы"

#. Link that navigates to the picture after this one.
#: templates/index.html
msgid "Next"
msgstr "Наступны"

#. Gray text shown in place of "Next" link, when the picture is the last one in an album
#: templates/index.html
msgid "Last"
msgstr "Апошні"

#. Link that lets a visitor save the original file
#: templates/index.html
msgid "Download"
msgstr "Запампаваць"

#. Control that shows the picture in original size
#: templates/index.html
msgid "Original size"
msgstr "Пачатковы памер"

#. A link that displays the photos in a slideshow.
#: templates/index.html
msgid "Slideshow"
msgstr "Слайдавая прэзэнтацыя"

#. A text that describes how to stop a slideshow.
#: templates/index.html
msgid "Click anywhere to stop the slideshow."
msgstr "Каб спыніць слайдавую прэзэнтацыю, націсьніце ў любым месцы."

#. From the line "Page [1] 2 3" below the view of thumbnails
#: templates/index.html
msgid "Page"
msgstr "Старонка"

#. Singular case
#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "1 image"
msgstr "1 малюнак"

#. Plural case
#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "{counter} images"
msgstr "{counter} малюнкі(аў)"

#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "and"
msgstr "і"

#. Singular case
#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "1 folder"
msgstr "1 тэчка"

#. Plural case
#. From the line "12 images and 3 folders" below the view of thumbnails
#: templates/index.html
msgid "{counter} folders"
msgstr "{counter} тэчкі (тэчак)"

#. From the line "Image 2 of 18" below the page view of a photo
#: templates/index.html
msgid "Image {imageIndex} of {imagesCount}"
msgstr "Малюнак {imageIndex} з {imagesCount}"

#. Message shown when the original share folder selected by the owner can't be accessed
#. Properties... text comes from the right-click menu of the application in the Unite panel.
#: templates/noSharedMountpoint.html
msgid ""
"Folder not found. To select a new one, right-click <STRONG>{serviceName}</"
"STRONG> in the Unite panel, and choose <STRONG>Properties</STRONG>"
msgstr ""
"Тэчка ня знойдзеная. Каб выбраць новую, націсьніце правай кнопкай мышы на "
"<STRONG>{serviceName}</STRONG> на панэлі Unite і выберыце "
"<STRONG>Уласьцівасьці</STRONG>"
